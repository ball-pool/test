package repository

import "gorm.io/gorm"

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{db}
}

type User struct {
	ID   int
	Name string
}

func (r *UserRepository) GetByID(id int) (User, error) {
	var u User
	err := r.db.Find(&u).Where("id = ?", id).Error
	return u, err
}
