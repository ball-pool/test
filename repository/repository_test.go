package repository_test

import (
	"regexp"
	"test/repository"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestGetWithSqlMock(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	db, _ := gorm.Open(mysql.New(mysql.Config{Conn: mockDB, SkipInitializeWithVersion: true}))
	r := repository.NewUserRepository(db)

	t.Run("success", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users`")).WillReturnRows(sqlmock.NewRows([]string{"id", "name"}).AddRow(87, "Simon"))

		u, err := r.GetByID(87)
		assert.NoError(t, err)
		assert.Equal(t, repository.User{ID: 87, Name: "Simon"}, u)
	})
}
