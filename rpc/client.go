package rpc

import (
	"google.golang.org/grpc"
)

type Client struct {
}

func (c *Client) Send() {
	conn, _ := grpc.NewClient("")
	defer conn.Close()
}
